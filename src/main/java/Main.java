package main.java;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import ventanas.VentanaInicioSesion;

/**
 * 
 * @author Alen
 *
 */
public class Main {
	private  static SistemaMapa sistema;
	public static void main(String[] args) {
		try {
			sistema = new SistemaMapa();
			VentanaInicioSesion ventana = new VentanaInicioSesion(sistema);
			ventana.setVisible(true);
		}catch(ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage()/*"Error al conectarse a la base de datos. Verifique que tiene una conexi�n a internet distinta y que el puerto 3306 est� abierto"*/, 
					"No se pudo ingresar a la base de datos",
                    JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}
	
	
}
