package main.java;

import java.sql.*;

public class Db {
	private static Connection conexion = null;
	private final String dominio = "localhost:3306";
	private final String baseDatos = "CubiGuiaTuristica";
	private final String nombreUsuario = "root";
	private final String contrasena = "cBc5536652";
	
	/**
	 * Genera conexión y registra el driver de la base de datos.
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * 
	 */
	private Db() throws SQLException, ClassNotFoundException{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		conexion = DriverManager.getConnection("jdbc:mysql://"+ dominio + "/"  +  baseDatos + "?useSSL=false", nombreUsuario, contrasena);		
		
	}
	
	
	public static Connection getConexion() throws ClassNotFoundException, SQLException {
		if(conexion == null)
			new Db();
		return conexion;
	}
}
