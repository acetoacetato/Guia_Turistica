package Usuarios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Colecciones.MapaComentariosUsuario;
import main.java.Comentario;

public class CuentaUsuario {
	private String nombreUsuario;
	private String contrasena;
	private String tipoCuenta;
	protected MapaComentariosUsuario comentariosMap;
	
	
	/**
	 * 
	 * @param usuario
	 * @param contrasena
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	public CuentaUsuario( )  {	}

	/**
	 * Constructor de una cuenta a partir de un ResultSet
	 * @param rs : Resultado de una consulta SQL.
	 * @throws SQLException
	 */
	public CuentaUsuario(ResultSet rs) throws SQLException {
	  setNombreUsuario(rs.getString("nombre"));
	  setContrasena(rs.getString("contrasena"));
	}

	
	public void cargarComentarios() throws ClassNotFoundException, SQLException {
		if(nombreUsuario == null)
			return;
		comentariosMap = new MapaComentariosUsuario(nombreUsuario);
	}
	
	/**
	 * Retorna el tipo de cuenta.
	 * @return Retorna un String, con el tipo de cuenta. 
	 */
  	public String getTipoCuenta() {
  		return tipoCuenta;
  	}
  	
  	
  	public void setTipoCuenta(String tipoCuenta) {
  		this.tipoCuenta = tipoCuenta;
  	}

  	

  	
  	/**
  	 * Obtiene cierta informacón de la cuenta.
  	 * @return Un string que conecta "cuenta " + el nombre de usuario.
  	 */
  	public String informacionCuenta() {
	  return getTipoCuenta() + " " + getNombreUsuario();
  	}

  	/**
  	 * Modifica un comentario.
  	 * @param comentAct : el comentario nuevo.
  	 * @param points : el nuevo puntaje.
  	 * @param lug : el lugar de donde se hizo el comentario.
  	 * @throws SQLException
  	 * @throws ClassNotFoundException 
  	 */
  	public void modificar(String comentAct, String points, String lug) throws SQLException, ClassNotFoundException {
		comentariosMap.modificar(comentAct,points,lug );
		
	}
  	
  	/**
  	 * obtiene todos los comentarios hechos por el usuario.
  	 * @return Un ArrayList<Comentario> con todos los comentarios del usuario.
  	 */
  	public ArrayList<Comentario> getComentarios() {
  		return comentariosMap.valores();
	}

  	
  	/**
  	 * Elimina un comentario.
  	 * @param comentario : una instancia del comentario a eliminar.
  	 */
  	public void eliminarComentario(Comentario comentario) {
	

		comentariosMap.eliminar(comentario.getPlaceId());
	
	}

  	
  	public void setNombreUsuario( String nombreUsuario ) {
		this.nombreUsuario = nombreUsuario;
	}

  	public String getNombreUsuario() {
		return nombreUsuario;
	}
  	
  	public void setContrasena( String contrasena ) {
		this.contrasena = contrasena;
	}
  
  	
	public String getContrasena() {
		return contrasena;
	}

	

}
	
