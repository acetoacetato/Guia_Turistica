package Usuarios;

import java.sql.SQLException;

public class UsuarioDirector {
	
	
	private UsuarioBuilder usuarioBuilder;
	
	
	
	public UsuarioDirector(UsuarioBuilder usuarioBuilder) {
		this.usuarioBuilder = usuarioBuilder;
	}
	
	public CuentaUsuario getCuentaUsuario() {
		return usuarioBuilder.getCuentaUsuario();
	}
	
	/**
	 * Crea al usuario
	 * @param nombreUsuario el nombre del usuario
	 * @param contrasena
	 * @return La misma instancia de UsuarioDirector
	 * @throws ClassNotFoundException 
	 * @throws SQLException
	 */
	public UsuarioDirector haceUsuario(String nombreUsuario, String contrasena) throws ClassNotFoundException, SQLException {
		usuarioBuilder.crearNuevoUsuario();
		usuarioBuilder.setCampos(nombreUsuario, contrasena);
		usuarioBuilder.cargarComentarios();
		return this;
	}
}
