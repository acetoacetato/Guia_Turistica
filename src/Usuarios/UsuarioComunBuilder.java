package Usuarios;

public class UsuarioComunBuilder extends UsuarioBuilder{

	
	public UsuarioComunBuilder() {
		this.crearNuevoUsuario();
	}
	
	public UsuarioBuilder setTipoCuenta() {
		usuario.setTipoCuenta("Usuario");
		return this;
	}
}
