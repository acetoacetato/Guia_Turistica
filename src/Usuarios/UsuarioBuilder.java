package Usuarios;

import java.sql.SQLException;

public abstract class UsuarioBuilder {
	
	protected CuentaUsuario usuario;
	
	/**
	 * establece el tipo de cuenta al deseado
	 * @return la misma instancia de UsuarioBuilder
	 */
	public abstract UsuarioBuilder setTipoCuenta();
	
	
	public CuentaUsuario getCuentaUsuario() { 
		return usuario; 
	}

	/**
	 * Crea la cuenta de usuario
	 */
	public void crearNuevoUsuario() {
		usuario = new CuentaUsuario();
	}
	
	
	public UsuarioBuilder setNombre(String nombreUsuario) {
		usuario.setNombreUsuario( nombreUsuario);
		return this;
	}
	
	public UsuarioBuilder setContrasena(String contrasena) {
		usuario.setContrasena(contrasena);
		return this;
	}
	
	
	/**
	 * Carga los comentarios del usuario
	 * @throws ClassNotFoundException En caso de que falle el registro del driver, se lanza la excepci�n.
	 * @throws SQLException En caso de no poder conectar a la base de datos, se lanza la excepci�n.
	 */
	public void cargarComentarios() throws ClassNotFoundException, SQLException {
		usuario.cargarComentarios();
	}
	
	
	/**
	 * Ingresa los campos del usuario, actualizando su nivel administrativo (usuario o administrador)
	 * @param nombreUsuario el nombre del usuario
	 * @param contrasena la contrase�a del usuario
	 */
	public void setCampos(String nombreUsuario, String contrasena) {
		setNombre(nombreUsuario);
		setContrasena(contrasena);
		setTipoCuenta();
	}
	
	
	
	
}
