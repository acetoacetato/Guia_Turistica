package Usuarios;

public class AdminBuilder extends UsuarioBuilder {

	public AdminBuilder() {
		this.crearNuevoUsuario();
	}
	
	
	public UsuarioBuilder setTipoCuenta() {
		usuario.setTipoCuenta("Administrador");
		return this;
	}
	
}
