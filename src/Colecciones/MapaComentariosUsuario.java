package Colecciones;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.Comentario;
import main.java.DbHandler;

public class MapaComentariosUsuario extends MapaComentarios {

	/**
	 * Constructor de los mapas del usuario
	 * @param id : id del usuario.
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	public MapaComentariosUsuario(String id) throws SQLException, ClassNotFoundException {
		importar();
	}
	
	/**
	 * importa los comentarios del ususairo desde la base de datos.
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	private void importar() throws SQLException, ClassNotFoundException {
		DbHandler db = DbHandler.getDbHandler();
		ResultSet rs = db.buscarComentariosUsuario(id);
		while(rs.next()) {
			mapaComentarios.putIfAbsent(rs.getString("id_lugar"), new Comentario(rs));
		}
	}

	
	

	
}
